#include "MLJECBase.h"

#include <cmath>
#include <cstdlib>

#include <CLHEP/Random/RandFlat.h>
#include <TFile.h>

#include <DataFormats/Common/interface/Ptr.h>
#include <DataFormats/JetReco/interface/GenJetCollection.h>
#include <FWCore/ParameterSet/interface/FileInPath.h>


MLJECBase::MLJECBase(edm::ParameterSet const &cfg) {
  auto const filter_path = cfg.getParameter<std::string>("filter_file");
  if (not filter_path.empty())
    jet_filter_.reset(new JetFilter(edm::FileInPath(filter_path).fullPath()));

  gen_jet_token_ = consumes<edm::View<reco::GenJet>>(
      cfg.getParameter<edm::InputTag>("gen_jets"));
  gen_flavor_token_ = consumes<reco::JetFlavourInfoMatchingCollection>(
      cfg.getParameter<edm::InputTag>("gen_flavors"));
  jet_token_ = consumes<edm::View<pat::Jet>>(
      cfg.getParameter<edm::InputTag>("jets"));
  pv_token_ = consumes<reco::VertexCollection>(
      cfg.getParameter<edm::InputTag>("pv"));
  sv_token_ = consumes<edm::View<reco::VertexCompositePtrCandidate>>(
      cfg.getParameter<edm::InputTag>("sv"));
  rho_token_ = consumes<double>(cfg.getParameter<edm::InputTag>("rho"));
  pileup_summary_token_ = consumes<edm::View<PileupSummaryInfo>>(
      cfg.getParameter<edm::InputTag>("pileup_info"));
}


void MLJECBase::analyze(edm::Event const &event, edm::EventSetup const &) {
  edm::Handle<reco::VertexCollection> primary_vertices;
  event.getByToken(pv_token_, primary_vertices);
  edm::Handle<edm::View<reco::VertexCompositePtrCandidate>> secondary_vertices;
  event.getByToken(sv_token_, secondary_vertices);
  edm::Handle<double> rho;
  event.getByToken(rho_token_, rho);
  edm::Handle<edm::View<PileupSummaryInfo>> pileup_summary;
  event.getByToken(pileup_summary_token_, pileup_summary);

  CLHEP::HepRandomEngine &rngEngine = rngService_->getEngine(event.streamID());

  num_pv_ = primary_vertices->size();
  rho_ = *rho;
  exp_pileup_ = pileup_summary->front().getTrueNumInteractions();

  edm::Handle<edm::View<reco::GenJet>> gen_jets;
  event.getByToken(gen_jet_token_, gen_jets);
  edm::Handle<reco::JetFlavourInfoMatchingCollection> flavor_map;
  event.getByToken(gen_flavor_token_, flavor_map);
  edm::Handle<edm::View<pat::Jet>> jets;
  event.getByToken(jet_token_, jets);

  for (int ijet = 0; ijet < int(gen_jets->size()); ++ijet) {
    auto const &gen_jet = gen_jets->at(ijet);
    auto const &flavor_info = (*flavor_map)[edm::Ptr<reco::Jet>(gen_jets, ijet)];
    hadron_flavor_ = flavor_info.getHadronFlavour();
    parton_flavor_ = flavor_info.getPartonFlavour();

    if (jet_filter_) {
      double const eff = jet_filter_->GetEfficiency(
          gen_jet, hadron_flavor_, parton_flavor_);
      if (CLHEP::RandFlat::shoot(&rngEngine) > eff)
        continue;
    }

    auto const *matched_jet = MatchJet(gen_jet, *jets);
    if (not matched_jet)
      continue;
    auto const &jet = *matched_jet;

    pt_gen_ = gen_jet.pt();
    eta_gen_ = gen_jet.eta();
    phi_gen_ = gen_jet.phi();
    mass_gen_ = gen_jet.mass();

    auto const p4_raw = jet.correctedP4("Uncorrected");
    pt_ = p4_raw.pt();
    eta_ = p4_raw.eta();
    phi_ = p4_raw.phi();
    mass_ = p4_raw.mass();
    pt_l1corr_ = jet.correctedP4("L1FastJet").pt();
    pt_full_corr_ = jet.pt();
    area_ = jet.jetArea();

    FillJet(
        jet, (num_pv_ > 0) ? &primary_vertices->at(0) : nullptr,
        *secondary_vertices);
    tree_->Fill();
  }
}


void MLJECBase::beginJob() {
  tree_ = file_service_->make<TTree>("Jets", "");

  tree_->Branch("pt_gen", &pt_gen_);
  tree_->Branch("eta_gen", &eta_gen_);
  tree_->Branch("phi_gen", &phi_gen_);
  tree_->Branch("mass_gen", &mass_gen_);
  tree_->Branch("hadron_flavor", &hadron_flavor_);
  tree_->Branch("parton_flavor", &parton_flavor_);

  tree_->Branch("num_pv", &num_pv_);
  tree_->Branch("rho", &rho_);
  tree_->Branch("exp_pileup", &exp_pileup_);

  tree_->Branch("pt", &pt_);
  tree_->Branch("pt_l1corr", &pt_l1corr_);
  tree_->Branch("pt_full_corr", &pt_full_corr_);
  tree_->Branch("eta", &eta_);
  tree_->Branch("phi", &phi_);
  tree_->Branch("mass", &mass_);
  tree_->Branch("area", &area_);

  AddBranches(tree_);
}


edm::ParameterSetDescription MLJECBase::CreateDescription() {
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("gen_jets")->setComment(
      "Collection of particle-level jets.");
  desc.add<edm::InputTag>("gen_flavors")->setComment(
      "Map with flavours of particle-level jets.");
  desc.add<edm::InputTag>("jets")->setComment(
      "Collection of reconstructed jets");
  desc.add<edm::InputTag>("pv")->setComment("Good primary vertices");
  desc.add<edm::InputTag>("sv")->setComment("Secondary vertices");
  desc.add<edm::InputTag>("rho")->setComment("Median angular pt density (rho)");
  desc.add<edm::InputTag>("pileup_info")->setComment("Pileup summary");
  desc.add<std::string>("filter_file", "")->setComment(
      "Path to file with filtering histograms");
  return desc;
}


bool MLJECBase::IsCharged(reco::Candidate const &cand) {
  int const abs_pdg_id = std::abs(cand.pdgId());
  return (abs_pdg_id == 11 or abs_pdg_id == 13 or abs_pdg_id == 211);
}


pat::Jet const *MLJECBase::MatchJet(
    reco::GenJet const &gen_jet, edm::View<pat::Jet> const &jets,
    double max_dr) {
  pat::Jet const *matched_jet{nullptr};
  double best_dr2 = std::pow(max_dr, 2);
  for (auto const &jet : jets) {
    double const dr2 = reco::deltaR2(gen_jet, jet);
    if (dr2 < best_dr2) {
      best_dr2 = dr2;
      matched_jet = &jet;
    }
  }
  return matched_jet;
}
