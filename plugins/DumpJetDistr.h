#ifndef ANALYSIS_JETNU_DUMPJETDISTR_H_
#define ANALYSIS_JETNU_DUMPJETDISTR_H_

#include <TH2.h>

#include <CommonTools/UtilAlgos/interface/TFileService.h>
#include <DataFormats/JetReco/interface/GenJet.h>
#include <FWCore/Framework/interface/EDAnalyzer.h>
#include <FWCore/Framework/interface/Event.h>
#include <FWCore/ParameterSet/interface/ParameterSet.h>
#include <FWCore/ParameterSet/interface/ConfigurationDescriptions.h>
#include <FWCore/ParameterSet/interface/ParameterSetDescription.h>
#include <FWCore/ServiceRegistry/interface/Service.h>
#include <SimDataFormats/JetMatching/interface/JetFlavourInfoMatching.h>


class DumpJetDistr : public edm::EDAnalyzer {
 public:
  DumpJetDistr(edm::ParameterSet const &cfg);

  void analyze(edm::Event const &event, edm::EventSetup const &) override;
  void beginJob() override;
  static void fillDescriptions(edm::ConfigurationDescriptions &descriptions);

 private:
  edm::EDGetTokenT<edm::View<reco::GenJet>> jet_token_;
  edm::EDGetTokenT<reco::JetFlavourInfoMatchingCollection> jet_flavor_token_;
  edm::Service<TFileService> file_service_;
  TH2D *hist_b_, *hist_c_, *hist_uds_, *hist_g_, *hist_unknown_;
};

#endif  // ANALYSIS_JETNU_DUMPJETDISTR_H_

