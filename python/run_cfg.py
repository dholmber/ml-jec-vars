import FWCore.ParameterSet.Config as cms
process = cms.Process('Analysis')
process.path = cms.Path()
process.task = cms.Task()


from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing('analysis')
options.setDefault('maxEvents', 1000)
options.setType('outputFile', VarParsing.varType.string)
options.setDefault('outputFile', 'sample.root')
options.register(
    'includeNu', False, VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    'Indicates whether neutrinos should be included in particle-level jets'
)
options.register(
    'saveHighLevel', False, VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    'Save high-level features instead of jet constituents'
)
options.parseArguments()

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(True)
)
process.maxEvents = cms.untracked.PSet(
    input=cms.untracked.int32(options.maxEvents))
process.load('FWCore.MessageLogger.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 1000

process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
from Configuration.AlCa.GlobalTag_condDBv2 import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '94X_mcRun2_asymptotic_v3')

process.source = cms.Source(
    'PoolSource',
    inputCommands = cms.untracked.vstring(
        'keep *', 'drop LHERunInfoProduct_*_*_*')
)
if options.inputFiles:
    process.source.fileNames = cms.untracked.vstring(options.inputFiles)
else:
    # Test file from a 'flat' QCD dataset
    process.source.fileNames = cms.untracked.vstring(
        '/store/mc/RunIISummer16MiniAODv3/QCD_Pt-15to7000_TuneCUETP8M1_Flat_13TeV_pythia8/MINIAODSIM/PUMoriond17_magnetOn_94X_mcRun2_asymptotic_v3-v2/40000/50BBFA70-AF20-E911-954D-001E67247936.root'
    )

process.RandomNumberGeneratorService = cms.Service(
    'RandomNumberGeneratorService',
    mlJECVars = cms.PSet(
        initialSeed = cms.untracked.uint32(3758),
        engineName = cms.untracked.string('TRandom3')
    ),
    mlJECConstituents = cms.PSet(
        initialSeed = cms.untracked.uint32(690),
        engineName = cms.untracked.string('TRandom3')
    )
)


# Primary vertices
from CommonTools.ParticleFlow.goodOfflinePrimaryVertices_cfi import \
    goodOfflinePrimaryVertices
process.goodPV = goodOfflinePrimaryVertices.clone(
    src=cms.InputTag('offlineSlimmedPrimaryVertices')
)
process.path += process.goodPV


# Veto prompt charged generator-level leptons
process.promptLeptons = cms.EDFilter(
    'CandViewSelector',
    src=cms.InputTag('prunedGenParticles'),
    cut=cms.string(
        '(abs(pdgId) == 11 | abs(pdgId) == 13 | abs(pdgId) == 15) & '
        'statusFlags().isPrompt'
    )
)
process.leptonVeto = cms.EDFilter(
    'PATCandViewCountFilter',
    src=cms.InputTag('promptLeptons'),
    minNumber=cms.uint32(0),
    maxNumber=cms.uint32(0)
)
process.path += cms.Sequence(process.promptLeptons * process.leptonVeto)


# Recluster generator-level jets
from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
if options.includeNu:
    gen_jet_source_label = 'packedGenParticles'
else:
    process.packedGenParticlesForJets = cms.EDFilter(
        'CandPtrSelector', src=cms.InputTag('packedGenParticles'),
        cut=cms.string('abs(pdgId) != 12 && abs(pdgId) != 14 && abs(pdgId) != 16')
    )
    process.path += process.packedGenParticlesForJets
    gen_jet_source_label = 'packedGenParticlesForJets'
process.ak4GenJets = ak4GenJets.clone(src=gen_jet_source_label, jetPtMin=0.)
process.task.add(process.ak4GenJets)


# Determine flavours of generator-level jets
from PhysicsTools.JetMCAlgos.HadronAndPartonSelector_cfi import \
    selectedHadronsAndPartonsForGenJetsFlavourInfos
from PhysicsTools.JetMCAlgos.AK4GenJetFlavourInfos_cfi import \
    ak4GenJetFlavourInfos
process.selectedHadronsAndPartonsForGenJetsFlavourInfos = \
    selectedHadronsAndPartonsForGenJetsFlavourInfos
process.ak4GenJetFlavourInfos = ak4GenJetFlavourInfos.clone(
    jets='ak4GenJets')
process.task.add(
    process.selectedHadronsAndPartonsForGenJetsFlavourInfos,
    process.ak4GenJetFlavourInfos
)


# Recluster reconstructed PF CHS jets
from PhysicsTools.PatAlgos.tools.jetTools import addJetCollection
from RecoJets.JetProducers.ak4PFJets_cfi import ak4PFJets
process.pfCHS = cms.EDFilter(
    'CandPtrSelector',
    src=cms.InputTag('packedPFCandidates'), cut=cms.string('fromPV'))
process.ak4PFJetsCHS = ak4PFJets.clone(
    src='pfCHS', doAreaFastjet=True, jetPtMin=0.)
addJetCollection(
    process,
    labelName = '',
    jetSource = cms.InputTag('ak4PFJetsCHS'),
    pvSource = cms.InputTag('offlineSlimmedPrimaryVertices'),
    pfCandidates = cms.InputTag('packedPFCandidates'),
    svSource = cms.InputTag('slimmedSecondaryVertices'),
    jetCorrections = (
        'AK4PFchs', ['L1FastJet', 'L2Relative', 'L3Absolute'], 'None'),
    genJetCollection = cms.InputTag('ak4GenJets'),
    genParticles = cms.InputTag('prunedGenParticles'),
    algo = 'AK',
    rParam = 0.4
)
process.path += process.pfCHS
process.task.add(process.ak4PFJetsCHS)


# Write jets to a tree
if options.saveHighLevel:
    process.mlJECVars = cms.EDAnalyzer(
        'MLJECVars',
        gen_jets=cms.InputTag('ak4GenJets'),
        gen_flavors=cms.InputTag('ak4GenJetFlavourInfos'),
        jets=cms.InputTag('patJets'),
        pv=cms.InputTag('goodPV'),
        sv=cms.InputTag('slimmedSecondaryVertices'),
        rho=cms.InputTag('fixedGridRhoFastjetAll'),
        pileup_info=cms.InputTag('slimmedAddPileupInfo'),
        filter_file=cms.string('Analysis/MLJECVars/data/jet_filtering.root')
    )
    process.path += process.mlJECVars
else:
    process.mlJECConstituents = cms.EDAnalyzer(
        'MLJECConstituents',
        gen_jets=cms.InputTag('ak4GenJets'),
        gen_flavors=cms.InputTag('ak4GenJetFlavourInfos'),
        jets=cms.InputTag('patJets'),
        pv=cms.InputTag('goodPV'),
        sv=cms.InputTag('slimmedSecondaryVertices'),
        rho=cms.InputTag('fixedGridRhoFastjetAll'),
        pileup_info=cms.InputTag('slimmedAddPileupInfo'),
        filter_file=cms.string('Analysis/MLJECVars/data/jet_filtering.root')
    )
    process.path += process.mlJECConstituents

from PhysicsTools.PatAlgos.tools.helpers import getPatAlgosToolsTask
process.path.associate(process.task)
process.path.associate(getPatAlgosToolsTask(process))

process.TFileService = cms.Service(
    'TFileService', fileName=cms.string(options.outputFile))

